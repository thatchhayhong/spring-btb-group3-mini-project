package keo.com.kh.practice_with_mybatis.service;

import keo.com.kh.practice_with_mybatis.model.Article;
import keo.com.kh.practice_with_mybatis.model.Subreddit;
import keo.com.kh.practice_with_mybatis.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ArticleServiceImp  implements ArticleService{




    ArticleRepository articleRepository ;

    @Autowired
  public   ArticleServiceImp(ArticleRepository articleRepository ){

        this.articleRepository = articleRepository;


    }



    @Override
    public List<Article> findAll() {

        return articleRepository.findAll();
    }

    @Override
    public List<Subreddit> allSubReddit() {
        return articleRepository.allSubreddits();
    }

    @Override
    public boolean addNewSub(String type , int id, String description) {
        return articleRepository.addNewSubReddit(type,id,description);
    }

    @Override
    public boolean addNewPost(int id, String title, String description, String imageUrl, int sub_id) {
        return articleRepository.addNewPost(id,title,description,imageUrl,sub_id);
    }


    @Override
    public Article findPost(int id) {
       return   articleRepository.findPostById(id);
    }


    @Override
    public int upVote(int upvote) {
        return articleRepository.upVoting(upvote);
    }


}
