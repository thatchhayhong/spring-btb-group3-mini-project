package keo.com.kh.practice_with_mybatis.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileStorageServiceImp implements FileStorageService{

    private Path fileStorageLocation;
    @Autowired
    public FileStorageServiceImp(){

        fileStorageLocation = Paths.get("src/main/resources/Images");
    }

    @Override
    public String saveFile(MultipartFile file) {

       try{

           String fileName = file.getOriginalFilename();
           if(fileName.contains("..")){

               System.out.println("Error with file name");
               return   null;

           }



           String[] fileParts = fileName.split("\\.");
           String extension = fileParts[1];

           fileName= UUID.randomUUID()+ "."+extension;

           // Image Location

           Path fileLocation= fileStorageLocation.resolve(fileName);

           Files.copy(file.getInputStream(),fileLocation, StandardCopyOption.REPLACE_EXISTING);
   return fileName;
       }catch (IOException ex){

           System.out.println("Error with the file ");
           return    null;
       }



    }
}
