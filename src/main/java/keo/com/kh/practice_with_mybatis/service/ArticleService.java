package keo.com.kh.practice_with_mybatis.service;

import keo.com.kh.practice_with_mybatis.model.Article;
import keo.com.kh.practice_with_mybatis.model.Subreddit;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArticleService {

    List<Article> findAll();
    List<Subreddit> allSubReddit();

    boolean addNewSub (String type, int id , String description) ;
    boolean addNewPost(int id, String title , String description , String imageUrl,int sub_id);

    Article findPost(int id );

    int upVote(int upvote);
}
