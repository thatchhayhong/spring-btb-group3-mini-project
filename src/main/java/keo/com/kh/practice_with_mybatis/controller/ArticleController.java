package keo.com.kh.practice_with_mybatis.controller;

import keo.com.kh.practice_with_mybatis.model.Article;
import keo.com.kh.practice_with_mybatis.model.Subreddit;
import keo.com.kh.practice_with_mybatis.service.ArticleService;
import keo.com.kh.practice_with_mybatis.service.FileStorageService;
import keo.com.kh.practice_with_mybatis.service.FileStorageServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Controller

public class ArticleController {

    // initiate the service

   public ArticleService articleService ;
   private FileStorageService fileStorageService;
   // public ArticleRepository articleRepository;

    //, ArticleRepository articleRepository
    @Autowired
    public ArticleController( ArticleService articleService ,FileStorageService fileStorageService ){
         this.articleService = articleService ;
         this.fileStorageService = fileStorageService;
        // this.articleRepository = articleRepository;
    }




    @GetMapping("/")
    public  String index(ModelMap modelMap){

      modelMap.addAttribute("subRedList", articleService.allSubReddit());

      modelMap.addAttribute("articleList",articleService.findAll());

        // I think there is is no need to app two model. But it's the easiest way to be able to display information
        return "index";
    }

    @GetMapping( "/display-all")
    public String displayAll(ModelMap modelMap){

        modelMap.addAttribute("articleList",articleService.findAll());
      //  System.out.println("All articles : "+ articleService.findAll());

        return "indexTest";
    }

    @GetMapping("/view-form")
    public String viewPostForm(ModelMap modelMap){
        modelMap.addAttribute("allSubReddit", articleService.allSubReddit());
      //  System.out.println("All article : "+ articleService.allSubReddit());
        return  "createPost";
    }

    @PostMapping("/create-post-handler")
    public String createPostHandler(@ModelAttribute Article article , @RequestParam(name= "file") MultipartFile file ){

        List<Article> allArticle = articleService.findAll();
        int id = allArticle.size()+1;
        article.setId(id);


        if (file.isEmpty()){

            article.setImageUrl("");
        } else  {

            try{

                article.setImageUrl("http://localhost:8081/photos/"+fileStorageService.saveFile(file));
                System.out.println("File Storage name : "+ fileStorageService.saveFile(file));

            }catch (IOException ex){

                System.out.println("Error IO Exception ");
            }

        }

       // article.setImageUrl("https://images-na.ssl-images-amazon.com/images/I/51jykC44gzL.jpg");



        System.out.println("Our Article : " + article);


        articleService.addNewPost(article.getId(),article.getTitle(),article.getDescription(),article.getImageUrl(), article.getSub_id() );
        return "redirect:/";
    }


    public String upVoteHandler(){
        return "index";
    }




    @GetMapping("/adding-subreddit-form")
    public String viewSubAddForm(){
        return "createSubReddit";
    }

    @PostMapping("/create_new_subreddit")
    public   String createSubReddit (@ModelAttribute Subreddit subreddit  ){
        // Model Attribute is used in order to receive all the information from the form we entered

        List<Subreddit> allSub = articleService.allSubReddit();
        System.out.println("value of all sub = "+ allSub.size());

        int id = allSub.size()+1 ;
//        subreddit.setType("New Type");

     //   System.out.println("Value of subreddit is : "+subreddit.toString());

        articleService.addNewSub(subreddit.getType(),id,subreddit.getDescription());
        return  "redirect:/";
    }



    // Viewing the detail of the post

// We are able to adding comment in the post detail
    @GetMapping("{id}/viewDetail")
    public String viewPostDetail(ModelMap model , @PathVariable int id ){

        // Post the chosen record to the other page .

      //  model.addAttribute("selectedPost",articleService.findPost(id));

        model.addAttribute("article",articleService.findPost(id));
        System.out.println("Selected Post value : "+articleService.findPost(id));

        return "viewPostDetail";
    }




    // liking and disliking
    @GetMapping("/upVoteHandler")

    public String upVoteMethod(@ModelAttribute Article article){

        int number_of_like=article.getUpvote() + 1;
        System.out.println("Number of like is :"+articleService.upVote(number_of_like));
        System.out.println("number_of_like"+ number_of_like);

        return "index" ;
    }

}
