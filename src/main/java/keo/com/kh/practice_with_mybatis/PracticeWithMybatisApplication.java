package keo.com.kh.practice_with_mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeWithMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(PracticeWithMybatisApplication.class, args);
    }

}
