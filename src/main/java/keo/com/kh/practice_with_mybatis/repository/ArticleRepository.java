package keo.com.kh.practice_with_mybatis.repository;


import keo.com.kh.practice_with_mybatis.model.Article;
import keo.com.kh.practice_with_mybatis.model.Subreddit;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {




//@Select("select a.id,a.title,a.\"imageUrl\",  a.sub_id  ,st.type ,a.description from article_tb as a inner join subreddit_tb st on a.id = st.article_id\n")

@Select("select * ,sub.type from article_tb as at inner join subreddit_tb sub on at.sub_id = sub.id order by at.id DESC")
public List<Article> findAll();

@Select("select * from subreddit_tb")
public List<Subreddit> allSubreddits();

@Insert("insert into subreddit_tb (type,id,description) values ( #{type},#{id},#{description} )")
public boolean addNewSubReddit(@Param("type") String type , int id , String description );

@Insert("insert into article_tb (id,title,description,\"imageUrl\",sub_id) values ( #{id},#{title},#{description},#{imageUrl},#{sub_id})")
public boolean addNewPost( int id,String title, String description, String imageUrl, int sub_id);


//    where at.id=#{id}
 //@Select("select * ,sub.type from article_tb as at inner join subreddit_tb sub on at.sub_id = sub.id where at.id = #{id} ;")

  @Select("select * ,sub.type from article_tb as at inner join subreddit_tb sub on at.sub_id = sub.id where at.id = #{id} ;")

 public  Article findPostById( @Param("id") int id);


 // @Insert("insert into article_tb (upvote) values (#{upvote})")
    @Update("update article_tb set upvote = #{upvote}")
  public int upVoting(int upvote);
}
