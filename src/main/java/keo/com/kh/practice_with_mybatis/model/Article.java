package keo.com.kh.practice_with_mybatis.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Article {

    private int id;
    private String  title;
    private String description  ;
    private  String imageUrl;

    // data of the subreddit
    private int sub_id;
    private String type;

    // number of vote

    private  int upvote;


}
